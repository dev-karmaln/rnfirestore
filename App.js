import React from 'react';
import AppNavigator from './src/navigations/AppNavigator';
import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import loginReducer from './src/store/reducers/login';
import ReduxThunk from 'redux-thunk';

const rootReducer = combineReducers({
    login: loginReducer
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const App = () => {

    return (
        <Provider store={store}>
            <AppNavigator />
        </Provider>
    );
};

export default App;