import { USER_LOGIN, USER_LOGOUT } from "../actions/login";

const initialState = {
    isLogin: false,
    userId: null
}

export default loginReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGIN:
            return {
                ...state,
                isLogin: action.user.isLogin,
                userId: action.user.userId
            }

        case USER_LOGOUT:
            return {
                ...state,
                isLogin: false,
                userId: null
            }
    }
    return state;
};