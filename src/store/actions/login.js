export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGOUT = "USER_LOGOUT";

export const userLogin = (isLogin, userId) => {
    return async dispatch => {
        try {
            dispatch({
                type: USER_LOGIN,
                user: {
                    isLogin,
                    userId
                }
            });
        } catch (err) {
            throw err;
        }
    }
};

export const userLogout = () => {
    return async dispatch => {
        try {
            dispatch({
                type: USER_LOGOUT
            });
        } catch (err) {
            throw (err);
        }
    }
}