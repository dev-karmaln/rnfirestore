import React from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native';

const Button = (props) => {
    return (
        <View>
            <TouchableOpacity
                style={{ ...styles.button, ...props.styles }}
                activeOpacity={0.6}
                onPress={props.onPress}>
                <Text style={styles.buttonText}>{props.label}</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    button: {
        width: "100%",
        justifyContent: "center",
        alignItems: 'center',
        padding: 10,
        borderRadius: 8
    },

    buttonText: {
        color: Colors.colorWhite,
        fontSize: 16,
        fontWeight: 'bold',
        letterSpacing: 1,
    }
});

export default Button;