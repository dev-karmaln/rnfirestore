import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    TextInput
} from 'react-native';
import Colors from '../constants/Colors';

const InputText = (props) => {

    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.label}>{props.label}</Text>
            </View>
            <View>
                <TextInput
                    style={styles.input}
                    onChangeText={props.onChangeText}
                    value={props.value}
                    placeholder={props.placeholder}
                    keyboardType={props.keyboardType}
                    secureTextEntry={props.secureTextEntry}
                    autoCorrect={false}
                    editable={!props.disable ? true : false}
                    autoCapitalize="none"
                    underlineColorAndroid='transparent'
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 8,
    },

    label: {
        fontSize: 18,
        color: Colors.colorDarkGray,
        margin: 0
    },

    input: {
        borderBottomColor: Colors.colorGray,
        borderBottomWidth: 1,
        color: Colors.colorBlack,
        margin: 0,
        padding: 2
    }

});

export default InputText;