import React, { useEffect, useState } from 'react';
import {
    ActivityIndicator,
    StyleSheet, View
} from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import Colors from '../constants/Colors';
import HomeBottomTabNavigator from './HomeBottomTabNavigator';
import AuthNavigator from './AuthNavigator';
import { useSelector } from 'react-redux';

const AppNavigator = () => {

    const [isLoading, setIsLoading] = useState(true);

    const user = useSelector(state => state.login);

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 1000);
    }, []);

    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="large" color={Colors.colorPrimary} />
            </View>
        )
    }

    return (
        <NavigationContainer>
            {
                user.isLogin ? (
                    <HomeBottomTabNavigator />
                ) : (
                    <AuthNavigator />
                )
            }
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({

});

export default AppNavigator;