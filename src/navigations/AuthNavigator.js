import React from "react";
import {
    Platform,
    StyleSheet,
} from "react-native";
import { createStackNavigator } from '@react-navigation/stack';
import Colors from '../constants/Colors';

import SigninScreen from "../Screens/SigninScreen";
import SignupScreen from "../Screens/SignupScreen";

const Stack = createStackNavigator();

const AuthNavigator = () => {
    return (
        <Stack.Navigator
            headerMode='none'
            initialRouteName="SigninScreen">
            <Stack.Screen
                name="SigninScreen"
                component={SigninScreen}
                options={() => ({
                    title: "Sign In",
                    headerStyle: {
                        backgroundColor: Platform.OS === 'android' ? Colors.colorPrimary : Colors.colorWhite,
                    },
                    headerTintColor: Platform.OS === 'android' ? Colors.colorWhite : Colors.colorPrimary,
                })} />

            <Stack.Screen
                name="SignupScreen"
                component={SignupScreen}
                options={() => ({
                    title: "Sign Up",
                    headerStyle: {
                        backgroundColor: Platform.OS === 'android' ? Colors.colorPrimary : Colors.colorWhite,
                    },
                    headerTintColor: Platform.OS === 'android' ? Colors.colorWhite : Colors.colorPrimary,
                })} />

        </Stack.Navigator>
    );
};

export default AuthNavigator;