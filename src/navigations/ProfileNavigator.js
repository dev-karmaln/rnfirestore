import React from "react";
import {
    StyleSheet
} from "react-native";
import { createStackNavigator } from '@react-navigation/stack';
import Colors from '../constants/Colors';
import ProfileScreen from '../Screens/ProfileScreen';

const Stack = createStackNavigator();

const UserNavigator = () => {
    return (
        <Stack.Navigator>

            <Stack.Screen
                name="ProfileScreen"
                component={ProfileScreen}
                options={({ route }) => ({
                    title: "Profile",
                    headerStyle: {
                        backgroundColor: Platform.OS === 'android' ? Colors.colorPrimary : Colors.colorWhite,
                    },
                    headerTintColor: Platform.OS === 'android' ? Colors.colorWhite : Colors.colorPrimary,
                })} />

        </Stack.Navigator>
    );
};

const styles = StyleSheet.create({

});

export default UserNavigator;