import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../constants/Colors';
import UserNavigator from "./UserNavigator";
import ProfileNavigator from "./ProfileNavigator";

const Tab = createBottomTabNavigator();

const HomeBottomTabNavigator = () => {
    return (
        <Tab.Navigator
            initialRouteName="UserNavigator"
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'UserNavigator') {
                        iconName = focused ? 'ios-chatbubbles-sharp' : 'ios-chatbubbles-outline';
                    } else if (route.name === 'ProfileNavigator') {
                        iconName = focused ? 'ios-person-circle' : 'ios-person-circle-outline';
                    }

                    return <Icon name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: Colors.colorPrimary,
                inactiveTintColor: "black",
            }}
        >

            <Tab.Screen
                name="UserNavigator"
                component={UserNavigator}
                options={{ title: "Chat" }} />

            <Tab.Screen
                name="ProfileNavigator"
                component={ProfileNavigator}
                options={{ title: "Profile" }} />

        </Tab.Navigator>
    );
};

export default HomeBottomTabNavigator;