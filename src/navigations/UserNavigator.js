import React from "react";
import {
    StyleSheet
} from "react-native";
import { createStackNavigator } from '@react-navigation/stack';
import Colors from '../constants/Colors';
import HomeScreen from '../Screens/HomeScreen';
import ChatScreen from "../Screens/ChatScreen";

const Stack = createStackNavigator();

const ProfileNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName="HomeScreen">

            <Stack.Screen
                name="HomeScreen"
                component={HomeScreen}
                options={({ route }) => ({
                    title: "Chats",
                    headerStyle: {
                        backgroundColor: Platform.OS === 'android' ? Colors.colorPrimary : Colors.colorWhite,
                    },
                    headerTintColor: Platform.OS === 'android' ? Colors.colorWhite : Colors.colorPrimary,
                })} />

            <Stack.Screen
                name="ChatScreen"
                component={ChatScreen}
                options={({ route }) => ({
                    title: route.params.title,
                    headerStyle: {
                        backgroundColor: Platform.OS === 'android' ? Colors.colorPrimary : Colors.colorWhite,
                    },
                    headerTintColor: Platform.OS === 'android' ? Colors.colorWhite : Colors.colorPrimary,
                })} />

        </Stack.Navigator>
    );
};

const styles = StyleSheet.create({

});

export default ProfileNavigator;