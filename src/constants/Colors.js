export default Colors = {
    colorPrimary: "#ffb037",
    colorWhite: "#FFFFFF",
    colorBlack: "#1b1717",
    colorInputBorder: "#364547",
    colorDarkGray: "#424642",
    colorGray: "#9e9d89"
};