import React, { useState } from "react";
import {
    ActivityIndicator,
    Alert,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    View
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import Colors from "../constants/Colors";
import Icon from 'react-native-vector-icons/Ionicons';
import InputText from "../components/InputText";
import Button from "../components/Button";

const SignupScreen = ({ navigation, route }) => {

    const ref = firestore().collection('Users');

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [isLoading, setIsLoading] = useState(false);

    const handleSignup = () => {
        setIsLoading(true);
        if (name.trim().length <= 0) {
            Alert.alert(
                "Validation Error",
                "Invalid Name"
            );
            setIsLoading(false);
            return;
        } else if (email.trim().length <= 0) {
            Alert.alert(
                "Validation Error",
                "Invalid Email"
            );
            setIsLoading(false);
            return;
        } else if (password.trim().length <= 6) {
            Alert.alert(
                "Validation Error",
                "Invalid Password"
            );
            setIsLoading(false);
            return;
        } else {
            _usreSignup(email, password);
        }
    }

    const _usreSignup = async (email, password) => {
        try {
            await auth().createUserWithEmailAndPassword(email, password).then((response) => {
                // console.log(response);
                if (response && response.user) {
                    _storeData(name, email.toString().toLowerCase());
                }
            })
        } catch (e) {
            setIsLoading(false);
            Alert.alert(
                "Error",
                e.message
            );
        }
    }

    const _storeData = async (name, email) => {
        await ref.doc(email).set({
            email: email,
            name: name,
            imageUrl: "https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png"
        }).then(() => {
            setName("");
            setPassword("");
            setEmail("");
            setIsLoading(false);
            Alert.alert("Success", "Account created successfully. Now you can Login.");
            navigation.goBack();
        }).catch((e) => {
            setIsLoading(false);
            Alert.alert("Error", e.message);
        });
    }

    return (
        <SafeAreaView>
            <ScrollView style={styles.root}>
                <View style={styles.container}>
                    <View style={styles.backButtonContainer}>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={styles.backButton} activeOpacity={0.5}>
                            <Icon name="ios-arrow-back-outline" size={30} color={Colors.colorPrimary} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.formContainer}>
                        <View style={styles.textContainer}>
                            <Text style={styles.textHeader}>Create an Account</Text>
                        </View>

                        <View style={{ marginTop: 32 }}>
                            <InputText
                                label="Name"
                                onChangeText={value => setName(value)}
                                value={name}
                                placeholder=""
                                keyboardType="default"
                                secureTextEntry={false}
                            />
                        </View>

                        <View style={{ marginTop: 16 }}>
                            <InputText
                                label="Email"
                                onChangeText={value => setEmail(value)}
                                value={email}
                                placeholder=""
                                keyboardType="email-address"
                                secureTextEntry={false}
                            />
                        </View>

                        <View style={{ marginTop: 16 }}>
                            <InputText
                                label="Password"
                                onChangeText={value => setPassword(value)}
                                value={password}
                                placeholder=""
                                keyboardType="default"
                                secureTextEntry={true}
                            />
                        </View>

                        <View style={{ marginTop: 48 }}>
                            <Button
                                styles={{ backgroundColor: Colors.colorPrimary }}
                                label="SIGNUP"
                                onPress={handleSignup} />

                            {
                                isLoading ? (
                                    <ActivityIndicator
                                        size="large"
                                        color={Colors.colorPrimary}
                                        style={{ marginTop: 8 }} />
                                ) : null
                            }
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    root: {
        backgroundColor: Colors.colorWhite,
        height: "100%"
    },

    container: {
        flex: 1
    },

    backButtonContainer: {
        marginStart: 16,
        width: 40
    },

    backButton: {
        backgroundColor: Colors.colorWhite,
        marginVertical: 16,
        borderRadius: 24,
        padding: 5,
        shadowColor: Colors.colorGray,
        shadowOffset: {
            height: 0.5,
            width: 0.5
        },
        elevation: 16
    },

    textContainer: {
        marginTop: 24
    },

    textHeader: {
        fontSize: 30,
        fontWeight: 'bold'
    },

    formContainer: {
        marginHorizontal: 32
    }
});

export default SignupScreen;