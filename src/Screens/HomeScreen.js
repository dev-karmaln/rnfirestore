// @refresh reset
import React, { useEffect, useState } from "react";
import {
    ActivityIndicator,
    StyleSheet,
    Text,
    View
} from "react-native";
import { AnimatedFlatList, AnimationType } from 'flatlist-intro-animations';
import firestore from '@react-native-firebase/firestore';
import Colors from "../constants/Colors";
import Utils from "../functions/Utils";
import UserItem from "../components/UserItem";
import { useDispatch } from "react-redux";
import * as loginAction from "../store/actions/login";

const HomeScreen = ({ navigation }) => {

    const ref = firestore().collection('Users');

    const [currentUser, setCurrentUser] = useState([]);
    const [users, setUsers] = useState([]);

    const [isLoading, setIsLoading] = useState(true);

    const dispatch = useDispatch();

    useEffect(() => {
        setIsLoading(true);
        Utils.getObjectData("USER_LOGIN").then(res => {
            if (res) {
                Utils.getObjectData("USER_DATA").then(result => {
                    if (!result) {
                        setIsLoading(false);
                        Utils.storeObjectData("USER_LOGIN", false);
                        Utils.storeObjectData("USER_DATA", null);
                        dispatch(loginAction.userLogin(false, null));
                    } else {
                        setCurrentUser(result);
                        getAllUsers(result);
                    }
                }).catch(err => {
                    setIsLoading(false);
                    alert(err.message);
                });
            } else {
                setIsLoading(false);
                Utils.storeObjectData("USER_LOGIN", false);
                Utils.storeObjectData("USER_DATA", null);
                dispatch(loginAction.userLogin(false, null));
            }
        }).catch(e => {
            setIsLoading(false);
            alert(e.message);
        });
    }, [navigation, dispatch]);

    const getAllUsers = async (user) => {
        await ref.get()
            .then(querySnapshot => {
                // console.log('Total users: ', querySnapshot.size);
                const list = [];
                querySnapshot.forEach(documentSnapshot => {
                    if (documentSnapshot.id != user.email) {
                        list.push(documentSnapshot.data());
                    }
                    // console.log('User ID: ', documentSnapshot.id, documentSnapshot.data());
                });
                setUsers(list);

                setTimeout(() => {
                    setIsLoading(false);
                }, 500);
            });
    }

    const showChat = (item) => {
        navigation.navigate("ChatScreen", {
            title: item.name,
            chatUser: item,
            currentUser
        })
    }

    if (users.length <= 0 && !isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.colorWhite }}>
                <Text style={{ marginHorizontal: 16, fontSize: 16, textAlign: 'center' }}>No Users are available for chatting. Tell your Friends to Sign Up.</Text>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            {
                isLoading ? (
                    <ActivityIndicator style={{ marginTop: 16 }} size="large" color={Colors.colorPrimary} />
                ) : (
                    <AnimatedFlatList
                        keyExtractor={item => item.email}
                        data={users}
                        renderItem={(itemData) => (
                            <UserItem
                                onClick={() => showChat(itemData.item)}
                                avtarUri={itemData.item.imageUrl}
                                name={itemData.item.name}
                                email={itemData.item.email} />
                        )}
                        animationType={AnimationType.SlideFromRight}
                        animationDuration={1000}
                        focused={true} />
                )
            }
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.colorWhite,
    },
});

export default HomeScreen;