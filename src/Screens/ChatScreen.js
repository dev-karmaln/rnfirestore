// @refresh reset
import React, { useCallback, useEffect, useLayoutEffect, useState } from 'react';
import {
    StyleSheet,
    View,
    LogBox
} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Colors from '../constants/Colors';
import Utils from '../functions/Utils';
import { GiftedChat } from 'react-native-gifted-chat';
import { useFocusEffect } from '@react-navigation/native'
import { useDispatch } from 'react-redux';

import * as loginAction from "../store/actions/login";

LogBox.ignoreLogs(["Warning: Encountered two children with the same key, `undefined`. Keys should be unique so that components maintain their identity across updates. Non-unique keys may cause children to be duplicated and/or omitted — the behavior is unsupported and could change in a future version."]);

const ChatScreen = ({ navigation, route }) => {

    const ref = firestore().collection('Chats');
    const dispatch = useDispatch();

    const { chatUser, currentUser } = route.params;

    const [messages, setMessages] = useState([]);

    const appendMessages = useCallback((messages) => {
        setMessages((previousMessages) => GiftedChat.append(previousMessages, messages));
    }, [messages]);

    useEffect(() => {
        const unsubscribe = ref.onSnapshot(querySnapshot => {
            const mesageFirestore = querySnapshot.docChanges()
                .filter(({ type }) => type === 'added')
                .map(({ doc }) => {
                    const message = doc.data();
                    if ((message.sender === currentUser.email && message.receiver === chatUser.email)
                        || (message.sender === chatUser.email && message.receiver === currentUser.email)) {
                        console.log(message)
                        return { ...message, createAt: message.createdAt };
                    }
                    return { ...messages }
                }).sort((a, b) => b.createAt - a.createAt);
            appendMessages(mesageFirestore);
        });

        return () => unsubscribe()
    }, []);

    useLayoutEffect(() => {
        Utils.getObjectData("USER_LOGIN").then(res => {
            if (res) {
                Utils.getObjectData("USER_DATA").then(result => {
                    if (!result) {
                        Utils.storeObjectData("USER_LOGIN", false);
                        Utils.storeObjectData("USER_DATA", null);
                        dispatch(loginAction.userLogin(false, null));
                    }
                }).catch(err => {
                    alert(err.message);
                });
            } else {
                Utils.storeObjectData("USER_LOGIN", false);
                Utils.storeObjectData("USER_DATA", null);
                dispatch(loginAction.userLogin(false, null));
            }
        }).catch(e => {
            alert(e.message);
        });
    }, []);

    useFocusEffect(
        useCallback(() => {

            // hide
            const parent = navigation.dangerouslyGetParent();
            parent.setOptions({
                tabBarVisible: false,
            });

            // reveal after changing screen
            return () =>
                parent.setOptions({
                    tabBarVisible: true,
                });

        }, []),
    );


    async function handleSend(messages) {

        messages[0].sender = currentUser.email
        messages[0].receiver = chatUser.email
        messages[0].createdAt = new Date().getTime()
        messages[0].user.avatar = currentUser.imageUrl
        messages[0].user._id = currentUser.email

        console.log(messages);
        const writes = messages.map(m => ref.add(m));
        await Promise.all(writes);
    }

    return (
        <View style={styles.container}>
            <GiftedChat
                listViewProps={{ contentContainerStyle: { 'flexGrow': 1, 'justifyContent': 'flex-end' } }}
                renderAvatarOnTop={true}
                messages={messages}
                user={currentUser}
                onSend={handleSend}
                user={{
                    _id: currentUser.email,
                }} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.colorWhite,
        flex: 1
    },
});

export default ChatScreen;