import React, { useEffect, useState } from "react";
import {
    ActivityIndicator,
    Alert,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import Button from "../components/Button";
import InputText from "../components/InputText";
import Colors from "../constants/Colors";
import Utils from "../functions/Utils";
import { useDispatch } from "react-redux";
import * as loginAction from "../store/actions/login";

const SigninScreen = ({ navigation, route }) => {

    const ref = firestore().collection('Users');

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [isLoading, setIsLoading] = useState(false);

    const dispatch = useDispatch();

    useEffect(() => {
        Utils.getObjectData("USER_LOGIN").then(res => {
            if (res) {
                Utils.getObjectData("USER_DATA").then(result => {
                    if (result) {
                        dispatch(loginAction.userLogin(true, result));
                    } else {
                        Utils.storeObjectData("USER_LOGIN", false);
                        Utils.storeStringData("USER_DATA", null);
                        dispatch(loginAction.userLogin(false, null));
                    }
                }).catch(err => {
                    alert(err.message);
                });
            } else {
                Utils.storeObjectData("USER_LOGIN", false);
                Utils.storeStringData("USER_DATA", null);
                dispatch(loginAction.userLogin(false, null));
            }
        }).catch(e => {
            alert(e.message);
        });
    }, []);

    const handleLogin = () => {
        setIsLoading(true);
        if (email.trim().length <= 0) {
            Alert.alert(
                "Validation Error",
                "Invalid Email"
            );
            setIsLoading(false);
            return;
        } else if (password.trim().length <= 6) {
            Alert.alert(
                "Validation Error",
                "Invalid Password"
            );
            setIsLoading(false);
            return;
        } else {
            _usreSignin(email, password);
        }
    };

    const _usreSignin = async (email, password) => {
        await auth().signInWithEmailAndPassword(email, password).then((response) => {
            if (response && response.user) {
                _getUserData(email.toString().toLowerCase());
            }
        }).catch((e) => {
            setIsLoading(false);
            Alert.alert(
                "Error",
                e.message
            );
        });
    }

    const _getUserData = async (userId) => {
        await ref.doc(userId).get().then((res) => {
            if (res._exists) {
                setIsLoading(false);
                if (Utils.storeObjectData("USER_LOGIN", true) && Utils.storeObjectData("USER_DATA", res._data)) {
                    dispatch(loginAction.userLogin(true, userId));
                } else {
                    Alert.alert("Error", "Data Storage Error.");
                }
            } else {
                Alert.alert("Error", "User Data not Exists...");
            }
        }).catch(e => {
            setIsLoading(false);
        });
    }

    const handleSignup = () => {
        navigation.navigate("SignupScreen");
    }

    return (
        <SafeAreaView>
            <ScrollView style={styles.root}>
                <View style={styles.container}>
                    <View style={styles.textContainer}>
                        <Text style={styles.textHeader}>Hi,</Text>
                        <Text style={styles.textHeader}>Welcome back !</Text>
                    </View>

                    <View style={styles.formContainer}>
                        <View>
                            <InputText
                                label="Email"
                                onChangeText={value => setEmail(value)}
                                value={email}
                                placeholder=""
                                keyboardType="email-address"
                                secureTextEntry={false}
                            />
                        </View>

                        <View style={{ marginTop: 16 }}>
                            <InputText
                                label="Password"
                                onChangeText={value => setPassword(value)}
                                value={password}
                                placeholder=""
                                keyboardType="default"
                                secureTextEntry={true}
                            />
                        </View>

                        <View style={{ marginTop: 48 }}>
                            <Button
                                styles={{ backgroundColor: Colors.colorPrimary }}
                                label="LOGIN"
                                onPress={handleLogin} />

                            {
                                isLoading ? (
                                    <ActivityIndicator
                                        style={{ marginTop: 8 }}
                                        size="large"
                                        color={Colors.colorPrimary} />
                                ) : null
                            }
                        </View>

                        <View style={styles.signupTextContainer}>
                            <Text>Don't have an Account?</Text>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                onPress={handleSignup}>
                                <Text style={styles.signupText}>
                                    Sign Up
                                </Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({

    root: {
        backgroundColor: Colors.colorWhite,
        height: "100%"
    },

    container: {
        flex: 1,
        marginHorizontal: 32
    },

    textContainer: {
        marginTop: 50
    },

    textHeader: {
        fontSize: 30,
        fontWeight: 'bold'
    },

    formContainer: {
        marginTop: 32
    },

    signupTextContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 48
    },

    signupText: {
        color: Colors.colorBlack,
        marginStart: 4,
        fontWeight: 'bold'
    }

});

export default SigninScreen;