import React, { useState, useEffect, useLayoutEffect } from "react";
import {
    Image,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    View,
    TouchableOpacity,
    Alert,
    ActivityIndicator
} from "react-native";
import firestore from '@react-native-firebase/firestore';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import storage from '@react-native-firebase/storage';
import Button from "../components/Button";
import InputText from "../components/InputText";
import Colors from "../constants/Colors";
import ImagePicker from 'react-native-image-crop-picker';
import Utils from "../functions/Utils";
import CustomHeaderButton from "../components/CustomHeaderButton";
import { useDispatch, useSelector } from "react-redux";
import * as loginAction from "../store/actions/login";

const ProfileScreen = ({ navigation }) => {

    const ref = firestore().collection('Users');

    const dispatch = useDispatch();

    const [imageUrl, setImageUrl] = useState("https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png");
    const [email, setEmail] = useState("");
    const [name, setName] = useState("");
    const [selectedImage, setSelectedImage] = useState("");

    const [isLoading, setIsLoading] = useState(false);


    useEffect(() => {
        getUser();
    }, [])

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                    <Item
                        title="Logout"
                        iconName={'ios-exit'}
                        onPress={() => {
                            Alert.alert(
                                "Logout",
                                "Are you sure you wants to Logout ??",
                                [
                                    {
                                        text: "OK",
                                        onPress: () => handleLogout()
                                    },
                                    {
                                        text: "CANCEL"
                                    }
                                ],
                                { cancelable: false }
                            );
                        }} />
                </HeaderButtons>
            )
        });
    }, [navigation]);

    const getUser = () => {
        Utils.getObjectData("USER_DATA").then((res) => {
            // console.log(res);
            if (res) {
                ref.doc(res.email).get().then((res) => {
                    // console.log(res);
                    if (res._exists) {
                        // console.log(res._data);
                        setImageUrl(res._data.imageUrl);
                        setEmail(res._data.email);
                        setName(res._data.name);
                    } else {
                        Alert.alert("Error", "User Data not Exists...");
                        handleLogout();
                    }
                });
            } else {
                handleLogout();
            }

        });
    }

    const handleUpdate = () => {
        setIsLoading(true);
        if (name.trim().length <= 6) {
            Alert.alert(
                "Validation Error",
                "Invalid Password"
            );
            setIsLoading(false);
            return;
        } else {
            if (selectImage == "") {
                _userUpdate(name);
            } else {
                uploadImage();
            }
        }
    }

    const selectImage = () => {
        ImagePicker.openPicker({
            width: 150,
            height: 150,
            cropping: true
        }).then(image => {
            setSelectedImage(image.path);
            setImageUrl(image.path);
        }).catch(e => {
            alert(e.message);
            setIsLoading(false);
        });
    }

    const _userUpdate = async (name, imageUrl = "") => {

        Utils.getObjectData("USER_DATA").then((res) => {
            if (res) {
                if (imageUrl === "") {
                    ref.doc(email).update({
                        name: name
                    }).then(() => {
                        Alert.alert(
                            "User Updated"
                        );
                        getUser();
                        setIsLoading(false);
                    }).catch((e) => {
                        Alert.alert(e.message);
                        setIsLoading(false);
                    });
                } else {
                    ref.doc(email).update({
                        name: name,
                        imageUrl: imageUrl
                    }).then(() => {
                        Alert.alert(
                            "User Updated"
                        );
                        getUser();
                        setIsLoading(false);
                    }).catch((e) => {
                        Alert.alert(e.message);
                        setIsLoading(false);
                    });
                }
            }
        }).catch(() => {
            setIsLoading(false);
        });
    }

    const uploadImage = async () => {
        const sessionId = new Date().getTime();
        const imageRef = storage().ref(`${sessionId}.png`);
        let task = imageRef.putFile(selectedImage);
        task.then((res) => {
            // console.log("Image Uploaded", res.state);
            if (res.state === "success") {
                _userUpdate(name, `https://firebasestorage.googleapis.com/v0/b/rnusermanagement.appspot.com/o/${res.metadata.fullPath}?alt=media`);
                getUser();
                setIsLoading(false);
            } else {
                Alert.alert(
                    "Image Upload Error",
                );
                setIsLoading(false);
            }
        }).catch((e) => {
            Alert.alert(e.message);
            setIsLoading(false);
        })
    }

    const handleLogout = () => {
        Utils.storeObjectData("USER_LOGIN", false);
        Utils.storeObjectData("USER_DATA", null);
        dispatch(loginAction.userLogin(false, null));
    }

    return (
        <SafeAreaView>
            <ScrollView style={styles.root}>
                <View style={styles.container}>

                    <View style={styles.imgContainer}>
                        <TouchableOpacity
                            onPress={selectImage}
                            activeOpacity={0.7}>
                            <Image
                                source={{ uri: imageUrl }}
                                style={styles.image} />
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginHorizontal: 32 }}>

                        <View style={{ marginVertical: 24 }}>
                            <InputText
                                label="Name"
                                onChangeText={value => setName(value)}
                                value={name}
                                placeholder=""
                                keyboardType="default"
                                secureTextEntry={false}
                            />
                        </View>

                        <View>
                            <InputText
                                label="Email"
                                disable={true}
                                onChangeText={value => setEmail(value)}
                                value={email}
                                placeholder=""
                                keyboardType="email-address"
                                secureTextEntry={false}
                            />
                        </View>

                        <View style={{ marginTop: 48 }}>
                            <Button
                                styles={{ backgroundColor: Colors.colorPrimary }}
                                label="UPDATE"
                                onPress={handleUpdate} />

                            {
                                isLoading ? (
                                    <ActivityIndicator
                                        size="large"
                                        color={Colors.colorPrimary}
                                        style={{ marginTop: 8 }} />
                                ) : null
                            }
                        </View>
                    </View>

                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    root: {
        backgroundColor: Colors.colorWhite,
        height: "100%"
    },

    container: {
        flex: 1
    },

    image: {
        height: 150,
        width: 150,
        borderRadius: 150,
        borderColor: Colors.colorDarkGray,
        borderWidth: 5
    },

    imgContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 32
    }
});

export default ProfileScreen;