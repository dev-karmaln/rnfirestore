import AsyncStorage from '@react-native-async-storage/async-storage';

const Utils = {
    storeStringData: async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
            return true;
        } catch (e) {
            // saving error
            console.log(e);
            return false;
        }
    },

    storeObjectData: async (key, value) => {
        try {
            const jsonValue = JSON.stringify(value);
            await AsyncStorage.setItem(key, jsonValue);
            return true;
        } catch (e) {
            // saving error
            console.log(e);
            return false;
        }
    },

    getStringData: async (key) => {
        try {
            const value = await AsyncStorage.getItem(key)
            return value != null ? value : null;
        } catch (e) {
            // error reading value
            console.log(e);
            return false;
        }
    },

    getObjectData: async (key) => {
        try {
            const jsonValue = await AsyncStorage.getItem(key)
            return jsonValue != null ? JSON.parse(jsonValue) : null;
        } catch (e) {
            // error reading value
            console.log(e);
            return false;
        }
    },
    formatDate: (date) => {
        let dateAndTimeArray = date.split(" ");
        let dateArray = dateAndTimeArray[0].split("-");
        let newDate = dateArray[1] + "-" + dateArray[0] + "-" + dateArray[2];
        let newDateAndTime = newDate + " " + dateAndTimeArray[1];

        return newDateAndTime;
    }
};

export default Utils;
