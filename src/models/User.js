class User {
    constructor(id, name, mobileNumber, email, password, profileUrl) {
        this.id = id;
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.password = password;
        this.profileUrl = profileUrl;
    }
}

export default User;